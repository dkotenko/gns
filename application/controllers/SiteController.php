<?php

namespace application\controllers;

use application\helpers\Film;
use application\models\FilmForm;
use application\models\FilmModel;
use Yii;
use yii\web\Controller;

/**
 * Class SiteController
 * @package application\controllers
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        $this->initBootstrap();
        $this->getView()->title = 'Films list';;

        echo $this->render('films_list', ['films' => (new FilmModel())->getAllFilmsAsArray()]);

        Yii::$app->end();
    }

    public function actionAddFilm()
    {
        $this->initBootstrap();
        $this->getView()->title = 'Add new film';

        echo $this->render('film_item', [
            'model' => new FilmForm()
        ]);

        Yii::$app->end();
    }

    public function actionCreateFilm()
    {
        $form = new FilmForm();
        if (Yii::$app->getRequest()->getIsPost() &&
            $form->load(Yii::$app->getRequest()->post(), 'FilmForm') &&
            $form->validate()
        ) {
            $model = new FilmModel();
            $model->setAttributes($form->getAttributes(), false);
            $model->save();

            Yii::$app->session->setFlash('success', 'Film added successfully');
        } else {
            Yii::$app->session->setFlash('error', current($form->getFirstErrors()));
        }

        $this->redirect('/');
        Yii::$app->end();
    }

    /**
     * Basic template parts initialisation
     */
    private function initBootstrap()
    {
        $this->getView()->registerMetaTag(['charset' => 'utf-8']);
        $this->getView()->registerMetaTag(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge']);
        $this->getView()->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']);

        $this->getView()->registerCssFile(Yii::getAlias('@webPath/public/css/bootstrap.min.css'));
        $this->getView()->registerCssFile(Yii::getAlias('@webPath/public/css/docs.min.css'));

        $this->getView()->registerJsFile(
            Yii::getAlias('@webPath/public/js/bootstrap.min.js'),
            ['depends' => ['yii\web\JqueryAsset']]
        );
    }
}
