<?php

namespace application\helpers;

/**
 * Class Film
 * @package application\helpers
 */
class Film
{
    const IS_ACTIVE = 1;
    const IS_INACTIVE = 0;

    /**
     * @var array
     */
    public static $status = [
        self::IS_INACTIVE => 'Inactive',
        self::IS_ACTIVE => 'Active'
    ];
}
