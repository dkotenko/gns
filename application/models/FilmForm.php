<?php

namespace application\models;

use Yii;
use yii\base\Model;

/**
 * Class FilmForm
 * @package application\models
 */
class FilmForm extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $year;

    /**
     * @var bool
     */
    public $isActive;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'year', 'isActive'], 'required'],
            ['name', 'string', 'min' => 3],
            ['year', 'integer', 'min' => 1500, 'max' => (int)date('Y')],
            ['isActive', 'boolean']
        ];
    }
}
