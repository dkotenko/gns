<?php

namespace application\models;

use application\helpers\Film;
use yii\db\ActiveRecord;

/**
 * Class FilmModel
 * @package application\models
 */
class FilmModel extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'Films';
    }

    /**
     * @return array
     */
    public function getAllFilmsAsArray()
    {
        $films = $this->find()
            ->where(['isActive' => Film::IS_ACTIVE])
            ->orderBy(['FIELD(year, id)' => SORT_DESC])
            ->all();

        $result = [];
        foreach ($films as $film) {
            /** @var \yii\db\ActiveRecord $film */
            $result[] = $film->getAttributes();
        }

        return $result;
    }
}
