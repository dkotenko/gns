<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $content */

$this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title><?php echo Html::encode($this->title); ?></title>
        <?php $this->head(); ?>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">GNS Test</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Active films</a></li>
                    <li><a href="/site/add-film">Add film</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="content" class="bs-docs-header" tabindex="-1">

    </div>
    <div class="container">
        <div class="container bs-docs-container">
            <div class="row">
                <div class="col-md-9" role="main">
                    <?php if (\Yii::$app->session->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo \Yii::$app->session->getFlash('success', null, true); ?>
                        </div>
                    <?php endif; ?>
                    <?php if (\Yii::$app->session->hasFlash('error')): ?>
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                <span aria-hidden="true">×</span>
                            </button>
                            <?php echo \Yii::$app->session->getFlash('error', null, true); ?>
                        </div>
                    <?php endif; ?>
                    <?php echo $content ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage();
