<?php

use application\helpers\Film;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \application\models\FilmForm $model */

?>
<div class="bs-docs-section">
    <h1 id="input-groups" class="page-header">Add new film</h1>
    <?php $form = ActiveForm::begin(['action' => '/site/create-film', 'method' => 'post']) ?>
        <div class="input-group">
            <span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-tag"></span></span>
            <?php echo $form->field($model, 'name')
                ->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'Name',
                    'aria-describedby' => 'sizing-addon2'
                ])
                ->label(false) ?>
        </div>

        <div class="input-group">
            <span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-calendar"></span></span>
            <?php echo $form->field($model, 'year')
                ->textInput([
                    'class' => 'form-control',
                    'placeholder' => 'Year',
                    'aria-describedby' => 'sizing-addon2'
                ])
                ->label(false) ?>
        </div>

        <div class="input-group">
            <span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-eye-open"></span></span>
            <?php echo $form->field($model, 'isActive')
                ->dropDownList(Film::$status, ['prompt' => 'check activity', 'class' => 'form-control'])
                ->label(false) ?>
        </div>
        <?php echo Html::submitInput('Submit', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
</div>
