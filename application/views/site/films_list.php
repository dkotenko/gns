<?php
/** @var array $films */
?>
<div class="bs-docs-section">
    <h1 id="input-groups" class="page-header">Allowed films</h1>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Year</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($films as $film) :
        ?>
        <tr>
            <th scope="row"><?php echo $film['id'] ?></th>
            <td><?php echo $film['name'] ?></td>
            <td><?php echo $film['year'] ?></td>
        </tr>
        <?php
        endforeach
        ?>
        </tbody>
    </table>
</div>
