<?php

return [
    'id' => 'gns-test-console',
    'basePath' => __DIR__ . '/../',
    'language' => 'en-US',
    'components' => [
        'db' => require(__DIR__ . '/../configs/database.php')
    ]
];
