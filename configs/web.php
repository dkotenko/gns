<?php

Yii::setAlias('@basePath', __DIR__ . '/..');
Yii::setAlias('@webPath', 'http://' . $_SERVER['HTTP_HOST']);
Yii::setAlias('@renderFile', __DIR__ . '/../application/views');

return [
    'id' => 'gns-test',
    'basePath' => __DIR__ . '/../',
    'controllerNamespace' => 'application\controllers',
    'viewPath' => 'application/views',
    'language' => 'en-US',
    'components' => [
        'db' => require(__DIR__ . '/../configs/database.php'),
        'request' => [
            'cookieValidationKey' => 'cookieKey',
            'enableCsrfValidation' => false,
            'baseUrl' => '/'
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'rules' => [
                '<controller>/<action>/<id:\d+>' => '<controller>/<action>',
                '<controller>/<action>/<name:[\w-]+>' => '<controller>/<action>'
            ],
            'showScriptName' => false,
            'scriptUrl' => '/'
        ],
        'assetManager' => [
            'basePath' => '@basePath/assets',
            'baseUrl' => '@webPath/assets',
        ]
    ]
];
