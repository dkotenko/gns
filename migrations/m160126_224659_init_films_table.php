<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_224659_init_films_table extends Migration
{
    const FILMS_TABLE = 'Films';

    public function up()
    {
        $this->createTable(self::FILMS_TABLE, [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'year' => Schema::TYPE_INTEGER,
            'isActive' => Schema::TYPE_BOOLEAN . ' DEFAULT true'
        ]);
    }

    public function down()
    {
        $this->dropTable(self::FILMS_TABLE);
    }
}
